libbio-das-lite-perl (2.11-9) unstable; urgency=medium

  * Team upload.
  * d/control: point the home page to CPAN. (Closes: #1081376)
  * d/control: declare compliance to standards version 4.7.0.
  * fix_test_failure.patch: normalize last update timestamp.

 -- Étienne Mollier <emollier@debian.org>  Sun, 01 Dec 2024 12:24:24 +0100

libbio-das-lite-perl (2.11-8) unstable; urgency=medium

  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 27 Nov 2020 11:36:44 +0100

libbio-das-lite-perl (2.11-7) unstable; urgency=medium

  * Respect DEB_BUILD_OPTIONS in override_dh_auto_test
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1

 -- Andreas Tille <tille@debian.org>  Wed, 24 Oct 2018 21:34:29 +0200

libbio-das-lite-perl (2.11-6) unstable; urgency=medium

  * Moved packaging from SVN to Git
  * Standards-Version: 4.1.1
  * debhelper 10
  * Secure URI in watch file

 -- Andreas Tille <tille@debian.org>  Tue, 21 Nov 2017 14:48:25 +0100

libbio-das-lite-perl (2.11-5) unstable; urgency=medium

  * Fix test suite (thanks for the patch to Alex Muntada <alexm@alexm.org>)
    Closes: #832845
  * cme fix dpkg-control

 -- Andreas Tille <tille@debian.org>  Mon, 08 Aug 2016 08:27:05 +0200

libbio-das-lite-perl (2.11-4) unstable; urgency=medium

  * Document duplicated tarball in source
    Closes: #811070

 -- Andreas Tille <tille@debian.org>  Fri, 15 Jan 2016 16:11:34 +0100

libbio-das-lite-perl (2.11-3) unstable; urgency=medium

  * Synchronise skipped tests for build and autopkgtest (Thanks for
    the patch to Niko Tyni <ntyni@debian.org>)
    Closes: #811025

 -- Andreas Tille <tille@debian.org>  Fri, 15 Jan 2016 09:33:16 +0100

libbio-das-lite-perl (2.11-2) unstable; urgency=medium

  * enable autopkgtest
    Closes: #810064

 -- Andreas Tille <tille@debian.org>  Wed, 06 Jan 2016 16:56:54 +0100

libbio-das-lite-perl (2.11-1) unstable; urgency=medium

  * New upstream version
  * Debian Med team maintenance
    Closes: #793959
  * source format 3.0 (quilt)
  * cme fix dpkg-control
  * debhelper 9
  * cme fix dpkg-copyright
  * remove additional files created by clean target
  * fix interpreter path in examples

 -- Andreas Tille <tille@debian.org>  Tue, 05 Jan 2016 19:40:33 +0100

libbio-das-lite-perl (2.04-1.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Add explicit build dependency on libmodule-build-perl
    Closes:  #789062 -- FTBFS with perl 5.22

 -- Damyan Ivanov <dmn@debian.org>  Fri, 24 Jul 2015 10:30:47 +0000

libbio-das-lite-perl (2.04-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "FTBFS: test failures":
    debian/rules:
    - unset http_proxy
    - skip tests that need internet access
    debian/control:
    - build depend on debhelper (>= 7.0.50~) because of the override
    (Closes: #613387)

 -- gregor herrmann <gregoa@debian.org>  Sat, 26 Nov 2011 16:24:55 +0100

libbio-das-lite-perl (2.04-1) unstable; urgency=low

  * Initial Release. (Closes: #592518)

 -- Richard Holland <holland@eaglegenomics.com>  Thu, 19 Aug 2010 18:19:24 +0200
